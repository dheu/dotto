source "$HOME/dot/conf/sh/env.sh"
source "$DOT/conf/bash/prompt.bash"

source "$DOT/conf/sh/aliases.sh"
source "$HOME/.local/share/fzf/shell/key-bindings.bash"
source "$HOME/.local/share/fzf/shell/completion.bash"

shopt -s histappend
shopt -s autocd
