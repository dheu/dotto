__heart() {
    if [[ "$?" == 0 ]]; then
        printf "\e[36m♥\e[0m "
    else
        printf "\e[0m♥\e[0m "
    fi  
}

__git_branch() {
    if git rev-parse --git-dir &> /dev/null ; then
        local cur_branch="$(git rev-parse \
            --abbrev-ref HEAD 2> /dev/null)"

        if [[ -n "$(git status | grep modified)" ]]; then
            git_info="\e[36m${cur_branch} *\e[0m "
        else
            git_info="\e[34m${cur_branch}\e[0m "
        fi
    fi  

    printf "$git_info"
}

__cur_dir() {
    if [[ ! ${PWD} == ${HOME} ]]; then
        if echo "${PWD}" | grep "$(echo ${HOME})" &> /dev/null ; then
            printf "$(echo ${PWD} | sed "s/\/home\/dheu\///") "
        else
            printf "${PWD} "
        fi
    fi
}

PS1="< \$(heart)\$(cur_dir)\$(git_branch)> "
PS2="+♥ "

export PS1 PS2
