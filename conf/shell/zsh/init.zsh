# INC="\
# ${DOTSHELLCONF}/zsh/env.zsh \
# ${DOTSHELLCONF}/sh/env.sh \
# ${DOTSHELLCONF}/sh/alias.sh \
# ${DOTSHELLCONF}/zsh/alias.zsh \
# ${DOTSHELLCONF}/sh/func.sh \
# ${DOTSHELLCONF}/zsh/conf.zsh \
# ${DOTSHELLCONF}/zsh/prompt/loli.zsh \
# ${HOME}/.local/share/fzf/shell/*.zsh \
# /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# for f in ${INC}; do
#     [[ -f $f ]] && . $f
# done

. "${DOTSHELLCONF}/zsh/env.zsh"
. "${DOTSHELLCONF}/sh/env.sh"
. "${DOTSHELLCONF}/sh/alias.sh"
. "${DOTSHELLCONF}/zsh/alias.zsh"
. "${DOTSHELLCONF}/sh/func.sh"

. "${DOTSHELLCONF}/zsh/conf.zsh"

. "${DOTSHELLCONF}/zsh/prompt/loli.zsh"

[[ -d /usr/share/doc/fzf ]] \
    && . /usr/share/doc/fzf/completion.zsh \
    && . /usr/share/doc/fzf/key-bindings.zsh

[[ -d /usr/share/zsh/plugins/zsh-syntax-highlighting ]] \
    && . "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

[[ -d /usr/share/zsh/plugins/zsh-autosuggestions ]] \
    && . "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
