HISTSIZE=10000
SAVEHIST=10000

export HISTSIZE SAVEHIST

FZF_DEFAULT_OPTS="--height 40% --layout=reverse"
