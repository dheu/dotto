__exitstat() {
    [[ $? == 0 ]] && return

    echo "%{${fg[blue]}%}$1%{${reset_color}%} "
}

__curdir() {
    local CUR

    case "${PWD}" in
        ${HOME})
            CUR="%{${fg[green]}%}⌂ " \
            ;;
        ${HOME}*)
            CUR="${PWD//${HOME}\/}"
            CUR=${CUR//\//%{${reset_color}%}\/\%{${fg[green]}%}}
            CUR=%{${fg[green]}%}${CUR}
            ;;
        /mnt/*)
            CUR="${PWD//\/mnt\/}"
            CUR=${CUR//\//%{${reset_color}%}\/\%{${fg[red]}%}}
            CUR=%{${fg[red]}%}${CUR}
            ;;
        *)
            CUR=${PWD//\//%{${reset_color}%}\/\%{${fg[blue]}%}}
            ;;
    esac

    printf "%s%s " "${CUR}" "%{${reset_color}%}"
}

__curbatperc() {
    local PERC STATUS

    STATUS="$(cat /sys/class/power_supply/BAT1/status)"
    PERC="$(cat /sys/class/power_supply/BAT1/capacity)"

    case ${STATUS} in
        Charging)
            PERC=%{${fg[green]}%}${PERC}
            ;;
        Discharging)
            PERC=%{${fg[red]}%}${PERC}
            ;;
        Full)
            return
            ;;
    esac

    printf " %s%%%%%s" "${PERC}" "%{${reset_color}%}"

}

__binclockh() {
    local binhour hour="$(date +%H)"
    (( hour++ ))

    bin32=({0..1}{0..1}{0..1}{0..1}{0..1})

    binhour="${bin32[${hour}]}"
    binhour="${binhour//0/·}"
    binhour="${binhour//1/•}"

    printf " %s" "${binhour}"
}

precmd() {
    PROMPT="$(__exitstat ♥)$(__curdir -r)> "
    RPROMPT="<$(__binclockh)$(__curbatperc)"
}
