[[ $- == *i* ]] || return

set -o vi

which clang &> /dev/null && CC="clang"

HISTFILE="${HOME}/.local/share/sh_hist"
INPUTRC="${DOTSHELLCONF}/sh/inputrc"

EDITOR="nvim -u ${DOTPROGCONF}/neovim/init.vim"

VISUAL="${EDITOR}"

PAGER="cat"
MANPAGER="less"
. ${DOTSHELLCONF}/sh/less_env.sh
LESSHISTFILE="-"

XAUTHORITY="${HOME}/.local/share/xauth"
XDG_DATA_HOME="${HOME}/.local/share"
# XDG_CONFIG_HOME="${HOME}/.local/conf"
XDG_CONFIG_HOME="${HOME}/.config"
XDG_DESKTOP_DIR="/dev/null"

# PYTHONUSERBASE="${XDG_DATA_HOME}/python"
GOPATH="${XDG_DATA_HOME}/go"
NODE_HOME="${XDG_DATA_HOME}/node"
CARGO_HOME="${XDG_DATA_HOME}/rust/cargo"
RUSTUP_HOME="${XDG_DATA_HOME}/rust/rustup"
# RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

WWW_HOME="${HOME}/.local/share/w3m"

MPLAYER="mpv"

LANG="en_US.UTF-8"

unset PATH LESSCOLOR
PATH="\
/usr/bin:\
${DOTBIN}:\
${DOTWMBIN}:\
${HOME}/bin:\
${NODE_HOME}/bin:\
${HOME}/.local/bin:\
${CARGO_HOME}/bin:\
${GOPATH}/bin"

export CC
export GOPATH CARGO_HOME RUSTUP_HOME RUST_SRC_PATH
export PATH INPUTRC LANG
export HISTFILE LESSHISTFILE XAUTHORITY WWW_HOME
export XDG_DATA_HOME XDG_CONFIG_HOME XDG_DESKTOP_DIR
export EDITOR VISUAL PAGER MANPAGER
