alias :q="exit"
alias :Q!="sudo shutdown"

alias lsn="exa --git-ignore --ignore-glob=System\ Volume\ Information"
alias ls="exa -a"
alias l="exa -al"
alias tar="bsdtar"

alias wtr="curl wttr.in/manila"
alias discord="chromium --app=https://discordapp.com/channels/@me"

alias gadd="git add ."
alias gomm="git commit -m"
alias gush="git push"

alias sc="scim"

alias gay="nougat --screenshot"

alias ptpb="curl -F c=@- https://ptpb.pw/"

alias cpick="colorpicker --preview"

alias v="${EDITOR}"

alias wget="wget \
    --hsts-file=${HOME}/.local/share/wget/hist"

alias irssi="irssi \
    --config=${DOTPROGCONF}/irssi.conf"
alias tremc="tremc \
    --config=${DOTPROGCONF}/tremc.conf"
alias tmux="tmux \
    -f ${DOTPROGCONF}/tmux.conf"
alias rtv="rtv \
    --config ${DOTPROGCONF}/rtv.conf"

alias newsboat="newsboat \
    --config-file=${DOTPROGCONF}/newsboat/config \
    --url-file=${DOTPROGCONF}/newsboat/urls"
alias zathura="zathura \
    --config-dir=${DOTPROGCONF}/zathura \
    --data-dir=${HOME}/.local/share/zathura"
alias calcurse="calcurse \
    --directory ${DOTPROGCONF}/calcurse"
alias ncmpcpp=" ncmpcpp \
    --config=${DOTPROGCONF}/ncmpcpp/conf \
    --bindings=${DOTPROGCONF}/ncmpcpp/bindings \
    --quiet"
alias rifle="rifle
    -c ${DOTPROGCONF}/ranger/rifle.conf"

for util in cp ln rm mv rename mkdir chmod chown; do
    alias "${util}"="${util} -v"
done
