lf() {
    if [ "$1" = "-f" ]; then
        shift
    elif [ -n "${RANGER_LEVEL}" ]; then
        echo "ranger level: ${RANGER_LEVEL}"
        return 1
    fi

    /usr/bin/ranger \
        --cachedir="${HOME}"/.cache/ranger \
        --datadir="${HOME}"/.local/share/ranger \
        --confdir="${DOTPROGCONF}"/ranger \
        "$@"
}

onion() {
    local curpwd="${PWD}"

    cd "${HOME}"/bin/tor-browser
    ./start-tor-browser.desktop
    cd "${curpwd}"
}
