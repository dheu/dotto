"rm
highlight   EndOfBuffer     ctermfg=0
highlight   SignColumn      ctermbg=0

highlight   Folded          ctermbg=0
set fillchars=fold:\ 

highlight   DiffAdd         ctermbg=0
highlight   DiffChange      ctermbg=0
highlight   DiffDelete      ctermbg=0   ctermfg=5

highlight   Colorcolumn     ctermbg=0
highlight   Cursorline      ctermbg=0

highlight   OverLength      ctermbg=5   ctermfg=0
match OverLength /\%81v./

highlight   VertSpli        ctermbg=0   ctermfg=14  cterm=NONE
highlight   Statusline                              cterm=NONE

highlight   LineNr          ctermbg=0   ctermfg=14  cterm=NONE

highlight   CursorLine      ctermbg=1               cterm=NONE

highlight   SignColumn      ctermbg=0

highlight   SpellBad        ctermbg=4   ctermfg=14
highlight   Visual          ctermbg=4

highlight clear SignColumn

highlight   MatchParen      ctermfg=14  ctermbg=0

highlight   Conceal         ctermfg=1  ctermbg=0
" highlight   SpellBad        ctermfg=7   ctermbg=11
highlight   SpellBad        ctermfg=7   ctermbg=0
highlight   DiffText        ctermfg=7   ctermbg=11
highlight   NvimInvalid     ctermfg=0   ctermbg=7
