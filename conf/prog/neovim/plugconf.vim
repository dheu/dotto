" ale "
"""""""
let g:ale_sign_error = '> '
let g:ale_sign_warning = '! '
highlight ALEErrorSign   ctermbg=11 ctermfg=7
highlight ALEWarningSign ctermbg=11 ctermfg=1
" highlight ALEErrorLine  ctermbg=7
highlight ALEWarning     ctermbg=11 ctermfg=1
highlight ALEWError      ctermbg=7

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_lint_on_text_changed = 'always'
let g:ale_lint_on_enter = 0
let g:ale_lint_on_save = 0

" let g:ale_fixers = {
" 			\ 'javascript': ['prettier'],
"             \ }

let g:ale_cpp_clang_options = '-std=c++17 -D VERSION=\"0000-tmp\" -Wall'
let g:ale_c_clang_options = '-std=c11 -D VERSION=\"0000-tmp\" -Wall'

let g:ale_asm_gcc_executable = 'nasm'
let g:ale_asm_gcc_options = '-Wall'

let g:ale_linters = {'rust': ['rls']}

" function! LinterStatus() abort
" 	let l:counts = ale#statusline#Count(bufnr(''))

" 	let l:all_errors = l:counts.error + l:counts.style_error
" 	let l:all_non_errors = l:counts.total - l:all_errors

" 	return l:counts.total == 0 ? 'OK' : printf(
" 	\   '%dW %dE',
" 	\   all_non_errors,
" 	\   all_errors
" 	\)
" endfunction
" set statusline=%{LinterStatus()}
"
" indent line "
"""""""""""""""

" let g:indentLine_setColors = 0
" let g:indentLine_color_tty_light = 11
" let g:indentLine_color_dark = 1 " (default: 2)

" let g:indentLine_setConceal = 0
" let g:indentLine_char = '│'
let g:indentLine_char = '▏'
" let g:indentLine_color_term = 4
let g:indentLine_color_term = 11

let g:vim_json_syntax_conceal = 0


" vim-signify "
"""""""""""""""
" let g:signify_realtime = 0
" let g:signify_line_highlight = 0

" pandoc "
""""""""""
" let g:pandoc#filetypes#handled = ['pandoc', 'markdown']
" let g:pandoc#filetypes#pandoc_markdown = 1
let g:pandoc#folding#use_foldtext = 0
let g:pandoc#formatting#textwidth = 80
let g:pandoc#formatting#mode = 'ha'
let g:pandoc#folding#level = 10
let g:pandoc#folding#fdc = 0
let g:pandoc#spell#default_langs = ['en','de']
let g:pandoc#modules#enabled = [
            \ 'formatting',
            \ 'folding',
            \ 'spell',
            \ 'keyboard',
            \ 'hypertext'
            \ ]

"             \ 'bibliographies',
"             \ 'completion',
"             \ 'metadata',
"             \ 'menu',
"             \ 'toc',
"             \ 'chdir',

" deoplete "
""""""""""""
let g:deoplete#enable_at_startup = 1
set completeopt-=preview
" let g:deoplete#auto_complete_delay = 1
" call deoplete#custom#option({
" 			\ 'ignore_case': v:true,
" 			\ 'min_pattern_length': 2,
" 			\ 'yarp': v:true,
" 			\ })

" emmet "
"""""""""
let g:user_emmet_install_global = 1
let g:user_emmet_leader_key='<C-E>'
" let g:user_emmet_settings = webapi#json#decode(join(readfile(expand('~/dot/conf/vim/snips/user_snippets.json')), "\n"))
"

" fzf "
"""""""
" let g:fzf_layout = { 'down': '~36%' }
" augroup fzfstatusline
" 	autocmd!
" 	function! s:fzf_statusline()
" 		highlight fzf1 ctermfg=161 ctermbg=251
" 		highlight fzf2 ctermfg=23 ctermbg=251
" 		highlight fzf3 ctermfg=237 ctermbg=251
" 		setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
" 	endfunction
" 	autocmd User FzfStatusLine call <SID>fzf_statusline()
" augroup end
" augroup diefzfstatusline
" 	autocmd!
" 	autocmd FileType fzf set laststatus=0 noshowmode noruler
" 		\ | autocmd BufLeave <buffer> set laststatus=2 showmode ruler
" augroup end

" mkdp "
""""""""
" let g:mkdp_auto_start = 0
" let g:mkdp_auto_open = 0
" let g:mkdp_auto_close = 1
" let g:mkdp_refresh_slow = 0
" let g:mkdp_command_for_global = 0

" goyo "
""""""""
" augroup goyo_enter
" 	autocmd!
" 	function! s:goyo_enter()
" 		set scl=no
" 	endfunction
" 	autocmd User GoyoEnter nested call <SID>goyo_enter()
" augroup end
" augroup goyo_leave
" 	autocmd!
" 	function! s:goyo_leave()
" 		set scl=auto
" 		source $DOTNVIMCONF/col.vim
" 	endfunction
" 	autocmd User GoyoLeave nested call <SID>goyo_leave()
" augroup end
