#!/usr/bin/bash

PREFIX="/tmp/ncmpcpp"

MUSIC_DIR="${HOME}/.mnt/adhoc/mu"
TMPCOVER="${PREFIX}/tmpcover.jpg"
COVER="${PREFIX}/cover.jpg"

{
    mkdir -p "${PREFIX}"
    rm -f "${COVER}"
    audfile="${MUSIC_DIR}/$(mpc --format %file% current)"

    ffmpeg -i "${audfile}" "${TMPCOVER}" &> /dev/null

    convert "${TMPCOVER}"       \
        -resize 300x            \
        -background "#101716"   \
        -extent 1100x400        \
        "${COVER}"

    # i still have no idea if this shit works...
    printf "\e]20;${COVER};100x100+0+0:op=keep-aspect\a"
} &
