#!/usr/bin/env sh

wew | while IFS=: read ev wid; do
    case $ev in
        # occurs when a window appears
        19)
            wattr o $wid || $HOME/dot/wm/wmutils/focus.sh $wid ;;
        # occurs when one disappears
        18)
            $HOME/dot/wm/wmutils/focus.sh $(lsw | head -n 1)
    esac
done
