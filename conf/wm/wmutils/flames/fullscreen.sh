#!/usr/bin/env sh

FSFILE=${FSFILE:-/tmp/wmutils/fsfile}

usage() {
    echo "usage: $(basename $0) <wid>"
    exit 1
}

[ -z "$1" ] && usage

if [ -f $FSFILE ]; then
    wtp $(cat $FSFILE)
    $HOME/dot/wm/wmutils/focus.sh $(cat $FSFILE | cut -d\  -f5)
    rm $FSFILE
else
    chwb -s 0 $1
    wattr xywhi $1 > $FSFILE
    wtp $(wattr xywh `lsw -r`) $1
fi 
