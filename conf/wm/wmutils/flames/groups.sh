#!/usr/bin/env sh

usage() {
    cat << EOF
usage: $(basename $0) [-hCU] [-c wid] [-s wid group] [-tmMu group]
       -h shows this help
       -c cleans WID from group files (and makes it visible)
       -C runs cleanup routine
       -s sets WID's group
       -t toggle group visibility state
       -m maps (shows) group
       -M maps group and unmaps all other groups
       -u unmaps (hides) group
       -U unmaps all the groups
EOF
    exit 1
}

[ $# -eq 0 ] && usage

GDIR=${GDIR:-/tmp/wmutils/groups}

# define our functions

# clean WID ($1) from group files
clean_wid() {
    t=$(mktemp /tmp/groups.XXXXXX)
    for x in $(ls $GDIR/group.*); do
        sed "/$1/d" "$x" >"$t"
        mv "$t" "$x"
    done
    rm -f "$t"
}

# cleans group ($1) from (in)active files
clean_status() {
    t=$(mktemp /tmp/groups.XXXXXX)
    sed "/$1/d" $GDIR/active >"$t"
    mv "$t" $GDIR/active
    sed "/$1/d" $GDIR/inactive >"$t"
    mv "$t" $GDIR/inactive
}

# shows all the windows in group ($1)
map_group() {
    # safety
    if ! grep -q $1 < $GDIR/all; then
        echo "Group doesn't exist"
        exit 1
    fi

    # clean statuses
    clean_status $1
    # add to active
    echo $1 >> $GDIR/active

    # loop through group and map windows
	xargs mapw -m <$GDIR/group.$1
}

# hides all the windows in group ($1)
unmap_group() {
    # safety
    if ! grep -q $1 < $GDIR/all; then
        echo "Group doesn't exist"
        exit 1
    fi

    # clean statuses
    clean_status $1
    # add to inactive
    echo $1 >> $GDIR/inactive

    # loop through group and unmap windows
    while read line; do
        mapw -u $line
    done < $GDIR/group.$1
}

# assigns WID ($1) to the group ($2)
set_group() {
    #so that neither grep nor ls in clean_wid complain
    #when group.$2 does not exist
    touch $GDIR/group.$2

    # make sure we've no duplicates
    clean_wid $1
    clean_status $2

    # insert WID into new group if not already there
    grep -q $1 < $GDIR/group.$2 \
        || echo $1 >> $GDIR/group.$2

    # if we can't find the group add it to groups and make it active
    grep -q $2 < $GDIR/all \
        || echo $2 >> $GDIR/all \
        && echo $2 >> $GDIR/active

    # map WID if group is active
    grep -q $2 < $GDIR/active \
        && mapw -m $1

    # unmap WID if group is inactive
    grep -q $2 < $GDIR/inactive \
        && mapw -u $1
}

# toggles visibility state of all the windows in group ($1)
toggle_group() {
    # safety
    if ! grep -q $1 < $GDIR/all; then
        echo "Group doesn't exist"
        return
    fi

    # search through active groups first
    grep -q $1 < $GDIR/active \
        && unmap_group $1 \
        && return

    # search through inactive groups next
    grep -q $1 < $GDIR/inactive \
        && map_group $1 \
        && return
}

# removes all the unexistent WIDs from groups
# removes all group files that don't exist
# removes from 'all' file all groups that don't exist
cleanup_everything() {
    # clean WIDs that don't exist
    # using `cat` instead of `<` because error suppression
    cat $GDIR/group.* 2>/dev/null | while read wid; do
        wattr $wid || \
        clean_wid $wid
    done

    # clean group files that are empty
    for file in $GDIR/group.*; do
        # is the group empty?
        if [ ! -s $file ]; then
            rm -f $file
        fi
    done

    # remove groups that don't exist from 'all'
    while read line; do
        if [ ! -f $GDIR/group.$line ]; then
            t=$(mktemp /tmp/groups.XXXXXX)
            sed "/$line/d" $GDIR/all >"$t"
            mv "$t" $GDIR/all
            clean_status $line
        fi
    done < $GDIR/all
}

# check $GDIR exists
[ -d $GDIR ] || mkdir -p $GDIR

# touch all the files
[ -f $GDIR/active ] || :> $GDIR/active
[ -f $GDIR/inactive ] || :> $GDIR/inactive
[ -f $GDIR/all ] || :> $GDIR/all

cleanup_everything

while getopts "hc:Cs:t:m:M:u:U" opt; do
    case $opt in
        h)
            usage;;
        c)
            clean_wid $OPTARG
            mapw -m $OPTARG
            break;;
        C)
            cleanup_everything
            break;;
        s)
            set_group $OPTARG $(eval echo "\$$OPTIND")
            break;;
        t)
            toggle_group $OPTARG
            break;;
        m)
            map_group $OPTARG
            break;;
        M)
            for file in $GDIR/group.*; do
                group=${file##*.}
                unmap_group $group
            done
            map_group $OPTARG
            break;;
        u)
            unmap_group $OPTARG
            break;;
        U)
            for file in $GDIR/group.*; do
                group=${file##*.}
                unmap_group $group
            done
            break;;
    esac
done
