#!/usr/bin/env sh

# get current window id
winactive=$(pfw)
ROOT=$(lsw -r)

bw=${BW:-13}
colactive=${ACTIVE:-0xFBC64D}
colinactive=${INACTIVE:-0x5D1516}

usage() {
    echo "usage: $(basename $0) <next|prev|wid>"
    exit 1
}

setborder() {
    # check if window exists
    wattr $2 || return

    # do not modify border of fullscreen windows
    test "$(wattr xywh $2)" = "$(wattr xywh $ROOT)" && return

    case $1 in
        active)
            chwb -s $bw -c $colactive $2;;
        inactive)
            chwb -s $bw -c $colinactive $2;;
    esac
}

case $1 in
    next)
        wid=$(lsw|grep -v $winactive|sed '1 p;d');;
    prev|previous)
        wid=$(lsw|grep -v $winactive|sed '$ p;d');;
    0x*)
        wattr $1 && wid=$1;;
    *)
        usage;;
esac

[ -z "$wid" ] \
    && echo "$(basename $0): can't find a window to focus" >&2 \
    && exit 1

[ $(wattr w $winactive) -eq $(wattr w $ROOT) ] && exit 1

setborder inactive $winactive # set inactive border on current window
setborder active $wid   # activate the new window
chwso -r $wid           # put it on top of the stack
wtf $wid                # set focus on it

wmp -a $(wattr xy $wid) # move the mouse cursor to
wmp -r $(wattr wh $wid) # .. its bottom right corner
