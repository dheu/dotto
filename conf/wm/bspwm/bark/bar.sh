#!/bin/bash

die() {
    pkill lemonbar
    exit 0
}

trap die SIGINT SIGKILL

BGROUND='#101716'
FGROUND='#30302F'

WM_NAME='bspwm'

BORDER='4'
WINGAP='10'
XOFFSET="$(( ${WINGAP}+${BORDER} ))"

HEIGHT='24'
WIDTH='1900'

BGDIM=${WIDTH}x${HEIGHT}+${WINGAP}+0

DIM=$(( ${WIDTH} - ${BORDER} - ${BORDER} ))x$(( ${HEIGHT} - ${BORDER} ))+${XOFFSET}+${YOFFSET}

BAR_FIFO=/tmp/bspwm_bar-socket

# lemonbar -d -p -b -g "${BGDIM}" -B "${FGROUND}"

# # music player
# while :; do
#     status=$(mpc status | sed -n '2p')

#     case "${status}" in
#         *playing*)
#             echo "M%{A:popup-top:}%{A3:pkill n30f:}$(mpc current)%{A}%{A}" > $BAR_FIFO
#             ;;
#         *paused*)
#             echo "M%{A:popup-top:}%{A3:pkill n30f:}$(mpc current)%{A}%{A}" > $BAR_FIFO
#             ;;
#         *)
#             echo "M Stopped" > $BAR_FIFO
#             ;;
#     esac
#     sleep 2;
# done  &

# # CLOCK
# while :; do
#     printf "C%s" "$(date '+%H:%M')" > $BAR_FIFO
#     sleep 0.1
# done &

# {
#     while : ; do
#         if read -r line < "${BAR_FIFO}"; then
#             case ${line} in
#                 C*)
#                     # clock output
#                     clock="%{F${FGROUND}}%{B${BGROUND}} ${line#?} %{B-}%{F-}"
#                     ;;
#                 M*)
#                     # music output
#                     music="%{F${FGROUND}}%{B${BGROUND}} ${line#?} %{B-}%{F-}"
#                     ;;
#             esac
#         fi
    
#         printf "%s\n" "%{c}${music}%{r}${clock}"
#     done 
# } | \
lemonbar \
    -b \
    -f 'scientifica:style=bold' \
    -g "${DIM}" \
    -B "${BGROUND}"
