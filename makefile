all: deps conf deploy

deps:
	./bin/install/deps.sh

conf:
	./bin/install/conf.sh

deploy:
	./bin/install/deploy
